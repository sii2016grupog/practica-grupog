/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import Entidades.Aviso;
import static Entidades.Orden.TiposEstado.abierto;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;


/**
 *
 * @author pablo
 */
@Named(value = "controlTrabajador")
@SessionScoped
public class ControlTrabajador implements Serializable {

    private List<Orden> orden;
    
    // Metodo que devuelve una lista con todas las ordenes de un trabajador.
    public List<Orden> getOrden(){
        orden = new ArrayList<Orden>();
        List<Trabajador> lista_trab;
        lista_trab = new ArrayList<Trabajador>();
        lista_trab.add(new Trabajador(789,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7890,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7891,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7892,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        Aviso av = new Aviso();
        Supervisor su = new Supervisor(123,"supervisor", "asdf", "asdf", "calle falsa 123", "111111111A",952111111);
        for(int i = 0; i < 10; i++){
            orden.add(new Orden((long)(i*i*i+1000),"Una orden",abierto,030516,040516,lista_trab,av,su));
        }
        return orden;
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        orden = null;
        return "login.xhtml";
    }
}
