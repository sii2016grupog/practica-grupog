/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import Entidades.Aviso;
import static Entidades.Orden.TiposEstado.abierto;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;


/**
 *
 * @author pablo
 */
@Named(value = "controlOT")
@SessionScoped
public class ControlOT implements Serializable {

    private Orden orden;
    private List<Trabajador> lista_trab;
    
    // Este metodo se llama al pulsar el boton modificar/ver orden. Prepara la orden que se va a presentar.
    public String setOrden() {
        lista_trab = new ArrayList<Trabajador>();
        lista_trab.add(new Trabajador(789,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7890,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7891,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7892,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        Avisos av = new Avisos();
        Supervisor su = new Supervisor(123,"supervisor", "asdf", "asdf", "calle falsa 123", "111111111A",952111111);
        orden = new Orden((long)000,"Una orden",abierto,130416,140516,lista_trab,av,su);
        return "OT.xhtml";
    }
    
    public void aniadirTrab(){
        lista_trab.add(new Trabajador(789,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
    }

    public Orden getOrden() {
        return orden;
    }

    public Long get_Id(){
        return orden.getId_Orden();
    }
    public String get_Descripcion(){
        return orden.getDescripcion();
    }
    public List<Trabajador> getTrabajadores(){
        return orden.getTrabajadores();
    }
    public int getInitialDate(){
        return orden.getFecha_inicio();
    }
    public int getFinalDate(){
        return orden.getFecha_fin();
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        orden = null;
        return "login.xhtml";
    }
}
