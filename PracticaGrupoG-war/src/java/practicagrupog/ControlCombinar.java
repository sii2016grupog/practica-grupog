/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import Entidades.Aviso;
import static Entidades.Orden.TiposEstado.*;
import static Entidades.Aviso.Urgencia.*;
import static Entidades.Aviso.TiposEstado.*;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Afectado;
import Entidades.Aviso.Urgencia;
import Entidades.Notificacion;
import Entidades.Operador;
import Entidades.Trabajador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;


/**
 *
 * 
 */
@Named(value = "controlCombinar")
@SessionScoped
public class ControlCombinar implements Serializable {

    private Aviso aviso, aviso1, aviso2;
    private List<Afectado> lista_afect;
    private List<Notificacion> lista_noti;
    private List<Orden> lista_orden;
    private List<Trabajador> lista_trab;
    private Operador operador_A ; 
    private Supervisor supervisor_su;
    
    public String setCombinar() {
        supervisor_su = new Supervisor(123,"Nombre del Supervisor", "asdf", "asdf", "calle falsa 123", "111111111A",952111111);

        operador_A = new Operador(456, "Nombre del Operador", "asdf", "asdf", "calle falsa 456", "222222222B", 952222222);
        
        lista_noti = new ArrayList<Notificaciones>();
        lista_noti.add(new Notificaciones(123, "Descripcion", "061215", "María", 0, null, operador_A, null));
        lista_noti.add(new Notificaciones(123, "Descripcion",  "061215", "María", 0, null, operador_A, null));
        lista_trab = new ArrayList<Trabajador>();
        lista_trab.add(new Trabajador(789,"trabajador1", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7890,"trabajador2", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7891,"trabajador3", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        lista_trab.add(new Trabajador(7892,"trabajador4", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
        
        
        lista_afect = new ArrayList<Afectados>();
        lista_afect.add(new Afectados(123L, "Nombre1", "Direccion1", 952123456, "", aviso));
        lista_afect.add(new Afectados(123L, "Nombre2", "Direccion2", 952987654, "", aviso));
        lista_orden = new ArrayList<Orden>();
        lista_orden.add(new Orden(001L, "Tubería averiada", Entidades.Orden.TiposEstado.abierto, 123, 456, lista_trab, aviso, supervisor_su));
        lista_orden.add(new Orden(002L, "No especificada", Entidades.Orden.TiposEstado.abierto, 123, 456, lista_trab, aviso, supervisor_su));
        aviso = new Avisos((long) (001), "Goteras de arriba", Urgencia.Urgente, Avisos.TiposEstado.enCola, null, lista_afect, lista_noti, null, null, operador_A);
        aviso1 = new Avisos((long) (002), "Cambio en infraestructura", Urgencia.Planificada, Avisos.TiposEstado.abierto, null, lista_afect, lista_noti, null, null, operador_A);        
        aviso2 = new Avisos((long) (003), "Por determinar", Urgencia.Urgente, Avisos.TiposEstado.enCola, null, lista_afect, lista_noti, null, null, operador_A);
        aviso.setAfectados(lista_afect);
        aviso.setOrdenes(lista_orden);
        /*Supervisor no tendr�a por qu� estar asignado a ninguna orden, s�lo a sus avisos.*/
        return "CombinarAvisos.xhtml";
    }
    
    /*Prueba a partir de aquí*/
    
    private boolean editmode;
    
    public void edit(){
        editmode = true;
    }
    
    public void save(){
        /*entityService.save(entity);*/
        editmode = false;
    }
    
    public boolean isEditmode(){
        return editmode;
    }
    
    /*Termina prueba*/
    
    public void aniadirTrab(){
        lista_trab.add(new Trabajador(789,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C",952333333));
    }  
    
    public List<Afectado> getAfectados(){
        return aviso.getAfectados();
    }
    
    public Entidades.Aviso.TiposEstado getEstado(){
        return aviso.getEstado();
    }
    
    public Entidades.Aviso.Urgencia getGrado_Urgencia(){
        return aviso.getGrado_Urgencia();
    }
    
    public List<Orden> getOrdenes(){
        return aviso.getOrdenes();
    }
    
    public Supervisor getSupervisor_su(){
        return supervisor_su;
    }
    
    public Operador getOperador_A(){
        return aviso.getOperador_A();
    }
    
    public Aviso getAviso(){
        return aviso;
    }
    
    public Aviso getAviso1(){
        return aviso1;
    }
    
    public Aviso getAviso2(){
        return aviso2;
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        aviso = null;
        return "login.xhtml";
    }
}
