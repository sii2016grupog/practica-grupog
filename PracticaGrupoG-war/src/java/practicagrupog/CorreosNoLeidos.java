/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import java.io.Serializable;

/**
 *
 * @author manuelgranadosmolina
 */
public class CorreosNoLeidos implements Serializable {
 
    private int id_correoNoLeido;
    private String email;
    private String detalles;

    CorreosNoLeidos(int id, String em, String detalle) {
        id_correoNoLeido = id;
        email = em;
        detalles = detalle;
    }

    public int getId_correoNoLeido() {
        return id_correoNoLeido;
    }

    public String getEmail() {
        return email;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setId_correoNoLeido(int id_correoNoLeido) {
        this.id_correoNoLeido = id_correoNoLeido;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    
    
}
