/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Aviso.TiposEstado;
import Entidades.Aviso.Urgencia;
import Entidades.Notificacion;
import Entidades.Operador;
import static Entidades.Orden.TiposEstado.abierto;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import Entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;

import Implementacion.app;
import javax.inject.Inject;

/**
 *
 * @author pablo
 */
@Named(value = "supervisorBean")
@SessionScoped
public class SupervisorBean implements Serializable {

    @Inject
    private app ap;
    @Inject
    private ControlAutorizacion ctrl;
    private List<Aviso> Avisos;
    private Aviso gAviso

    public List<Aviso> getNavisos() {
        return ap.getNAvisos();
    }

    public List<Aviso> getLavisos() {
        List<Aviso> lavisos = new ArrayList<>();
        List<Aviso> aux = ap.getLAvisos();

        for (Aviso av : aux) {
            if (this.ctrl.getUsuario() == av.getSupervisor_A()) {
                lavisos.add(av);
            }
        }

        return lavisos;
    }

    public List<Aviso> getCavisos() {
        return ap.getCAvisos();
    }

    public List<Aviso> geAvisos() {
        return ap.getAllAvisos();
    }

    public String unirAvisos() {
        return "index.xhtml";
    }

    public List<Aviso> setBusqueda() {
        List<Aviso> prueba;
        prueba=(List<Aviso>) ap.buscaAviso(gAviso);
        return prueba;
    }
    
    public String nextPage(){
        return "busqueda1.xhtml";
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        return "login.xhtml";
    }
}
