/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Aviso.TiposEstado;
import Entidades.Aviso.Urgencia;
import Entidades.Notificacion;
import Entidades.Operador;
import static Entidades.Orden.TiposEstado.abierto;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;

/**
 *
 * @author pablo
 */
@Named(value = "controlAfectado")
@SessionScoped
public class ControlAfectado implements Serializable {

    @Inject
    private app ap;
    @Inject
    private ControlAutorizacion ctrl;
    private List<Afectado> afec;

    public List<Afectado> setBusqueda() {
        List<Afectado> prueba;
        prueba=(List<Afectado>) ap.getAfectados(afectado);
        return prueba;
    }

    public String nextPage(){
        return "afectados1.xhtml";
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        return "login.xhtml";
    }
   

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        orden = null;
        avisos = null;
        afect = null;
        notificaciones = null;
        operador_A = null;
        return "login.xhtml";
    }
}
