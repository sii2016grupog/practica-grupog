/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

import java.io.Serializable;

/**
 *
 * @author manuelgranadosmolina
 */
public class CorreosLeidos implements Serializable {

    private int id_correoLeido;
    private String email;
    private String detalles;

    CorreosLeidos() {
    }

    CorreosLeidos(int id, String em, String detalle) {
        id_correoLeido = id;
        email = em;
        detalles = detalle;
    }

    public int getId_correoLeido() {
        return id_correoLeido;
    }

    public String getEmail() {
        return email;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setId_correoLeido(int id_correoLeido) {
        this.id_correoLeido = id_correoLeido;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

}
