/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practicagrupog;

// cambiar al paquete donde esté el usuario y los roles
import Entidades.Operador;
import Entidades.Supervisor;
import Entidades.Trabajador;
import Entidades.Usuario;
import Entidades.Usuario.Rol;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Implementacion.app;

/**
 *
 * @author francis
 */
@Named(value = "login")
@RequestScoped

public class Index {
    // cambiar a la carpeta donde estén los roles

    
    private String usuario;
    private String contrasenia;
    private List<Usuario> usuarios;
    
    @Inject
    private ControlAutorizacion ctrl;

    @EJB
    private app app;
    /**
     * Creates a new instance of Login
     */
    public Index() {
        usuarios = new ArrayList<Usuario>();
        usuarios.add(new Supervisor(123,"supervisor", "asdf", "asdf", "calle falsa 123", "111111111A","952111111"));
        usuarios.add(new Operador(456,"operador", "asdf", "asdf", "calle falsa 456", "222222222B","952222222"));
        usuarios.add(new Trabajador(789,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C","952333333"));
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String autenticar() {
        app.Error e = app.compruebaLoginPrincipal(usuario,contrasenia);
        
        System.out.println("......................................................................");
        FacesMessage fm;
        
        switch (e) {
            case NO_ERROR:
                Usuario us=app.refrescarUsuarioPrincipal(usuario);
                
                ctrl.setUsuario(us);
                System.out.println("......................................................................");
        
                switch(ctrl.getUsuario().getRol())
                {
                    case SUPERVISOR: 
                        return "supervisor.xhtml";
                    case OPERADOR:
                        return "operador.xhtml";
                    case TRABAJADOR:
                        return "trabajador.xhtml";
                }
            case CUENTA_INEXISTENTE:
                fm = new FacesMessage("La cuenta no existe");
                FacesContext.getCurrentInstance().addMessage("login:user", fm);
                break;

            case CONTRASENIA_INVALIDA:
                fm = new FacesMessage("La contraseña no es correcta");
                FacesContext.getCurrentInstance().addMessage("login:pass", fm);
                break;
            default:
                fm = new FacesMessage("Error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, fm);
                break;
        }

        return null;
    }
}
