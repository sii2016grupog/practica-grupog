package practicagrupog;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manuelgranadosmolina
 */

import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Notificacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.FlowEvent;
 
@Named(value = "controlOperador")
@SessionScoped
public class ControlOperador implements Serializable {
    private Notificacion notificaciones = new Notificacion();
    private Aviso avisos = new Aviso();
    private Afectado afectados = new Afectado();
    private List<CorreosNoLeidos> correosNoLeidos;
    private List<CorreosLeidos> correosLeidos;
    
    public Notificacion getNotificaciones() {
        return notificaciones;
    }
 
    public void setNotificaciones(Notificacion notificaciones) {
        this.notificaciones = notificaciones;
    }

    public Aviso getAvisos() {
        return avisos;
    }

    public void setAvisos(Aviso avisos) {
        this.avisos = avisos;
    }

    public Afectado getAfectados() {
        return afectados;
    }

    public void setAfectados(Afectado afectados) {
        this.afectados = afectados;
    }
    
    public List<CorreosNoLeidos> getCorreosNoLeidos() {
        correosNoLeidos = new ArrayList<CorreosNoLeidos>();
        correosNoLeidos.add(new CorreosNoLeidos(1, "manuel@uma.es", "Averia en la calle de la victoria"));
        correosNoLeidos.add(new CorreosNoLeidos(2, "pablo@uma.es", "Averia en la calle de la salud"));
        correosNoLeidos.add(new CorreosNoLeidos(3, "manolo@uma.es", "Averia en la avenida de la paloma"));
        return correosNoLeidos;
    }
    
    public List<CorreosLeidos> getCorreosLeidos() {
        correosLeidos = new ArrayList<CorreosLeidos>();
        correosLeidos.add(new CorreosLeidos(4, "guille@uma.es", "Averia en la calle de la victoria"));
        correosLeidos.add(new CorreosLeidos(5, "iker@uma.es", "Averia en la plaza de la merced"));
        correosLeidos.add(new CorreosLeidos(6, "manolo@uma.es", "Averia en la avenida de la paloma"));
        return correosLeidos;
    }
 
    public String onFlowProcess(FlowEvent event) {
            return event.getNewStep();
    }
    
    public String crear() {
        FacesMessage msg = new FacesMessage("Confirmado", "Notificacion creada correctamente :" + notificaciones.getId_notificaciones());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        FacesMessage msg1 = new FacesMessage("Confirmado", "Aviso creado correctamente :" + avisos.getNombre());
        FacesContext.getCurrentInstance().addMessage(null, msg1);
        FacesMessage msg2 = new FacesMessage("Confirmado", "Afectado creado correctamente :" + notificaciones.getId_notificaciones());
        FacesContext.getCurrentInstance().addMessage(null, msg2);
        
        
         // NControlOperador.Error e = nControlOperador.insertarNotificacion(notificaciones);
        //NControlOperador.Error e1 = nControlOperador.insertarAviso(avisos);
       // NControlOperador.Error e2 = nControlOperador.insertarAfectado(afectados);
        //addMessage("Confirmado", "Notificación creada.");
        //addMessage("Confirmado", "Aviso creado.");
        //addMessage("Confirmado", "Afectado creado.");
        //notificaciones.setDescripcion(null);
        //notificaciones.setFecha(null);
        //notificaciones.setNotificador(null);
        //notificaciones.setTlf_notificador(0);
        //notificaciones.setEmail(null);
        //avisos.setNombre(null);
        //avisos.setGrado_Urgencia(null);
        //avisos.setEstado(null);
        //avisos.setInforme(null);
        //afectados.setNombre(null);
        //afectados.setDireccion(null);
        //afectados.setTelefono(0);
        //afectados.setDisponibilidad(null);
        
        return "operador.xhtml"; 
    }
     
}