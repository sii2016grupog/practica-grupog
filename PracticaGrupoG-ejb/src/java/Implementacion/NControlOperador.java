package Implementacion;

import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Notificacion;
import javax.ejb.Local;

/**
 *
 * @author manuelgranadosmolina
 */
@Local
public interface NControlOperador {
    
    public static enum Error {

        NO_ERROR
    };

    public Error insertarNotificacion(Notificaciones n);
    public Error insertarAviso(Avisos a);
    public Error insertarAfectado(Afectados f);
}