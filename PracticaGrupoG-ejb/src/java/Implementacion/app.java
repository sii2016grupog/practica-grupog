/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Implementacion;

import Entidades.Aviso;
import Entidades.Usuario;
import Entidades.Afectado;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Pablo
 */
@Local
public interface app {

    public static enum Error {
        CONTRASENIA_INVALIDA,
        CUENTA_INEXISTENTE,
        NO_ERROR
    };

    public Usuario refrescarUsuario(Usuario us);

    public Error compruebaLoginPrincipal(String dni, String pass);

    public Error compruebaLogin(Usuario u);

    public List<Usuario> listaUsuario();

    public Usuario refrescarUsuarioPrincipal(String nombre);
    
    public List<Aviso> getNAvisos();

    public List<Aviso> getLAvisos();

    public List<Aviso> getCAvisos();
    
    public List<Aviso> getAllAvisos();
    
    public Afectado getAfectados(Afectado a);
    
    public Aviso buscaAviso(Aviso a);

}
