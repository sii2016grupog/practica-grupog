/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Implementacion;

import Entidades.Aviso;
import Entidades.Usuario;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
public class appImpl implements app {

    @PersistenceContext(unitName = "Test-ejbPU")
    private EntityManager em;

    @Override
    public Usuario refrescarUsuario(Usuario us) {
        Error e = compruebaLogin(us);
        if (e != Error.NO_ERROR) {
            return null;
        }

        Usuario user = em.find(Usuario.class, us.getId_Usuario());
        em.refresh(user);
        return user;
    }

    @Override
    public Error compruebaLoginPrincipal(String nombre, String pass) {
        Usuario user = null;
        TypedQuery<Usuario> tq = em.createQuery("SELECT u FROM Usuario u", Usuario.class);
        for (Usuario u : tq.getResultList()) {
            if (nombre.equals(u.getNombre()) && pass.equals(u.getContrasenia())) {
                user = u;
            }
        }

        if (user == null) {

            return Error.CUENTA_INEXISTENTE;
        }

        if (user.getContrasenia().equals(pass)) {
            return Error.NO_ERROR;
        } else {
            return Error.CONTRASENIA_INVALIDA;
        }
    }

    @Override
    public List<Usuario> listaUsuario() {
        Query query = em.createQuery("SELECT u FROM Usuario");
        return (List<Usuario>) query.getResultList();
    }

    @Override
    public Error compruebaLogin(Usuario u) {
        Usuario user = em.find(Usuario.class, u.getId_Usuario());
        if (user == null) {
            return Error.CUENTA_INEXISTENTE;
        }

        if (user.getContrasenia().equals(u.getContrasenia())) {
            return Error.NO_ERROR;
        } else {
            return Error.CONTRASENIA_INVALIDA;
        }
    }

    @Override
    public Usuario refrescarUsuarioPrincipal(String nombre) {
        //Query q = em.createNamedQuery("Usuario.usuario").setParameter("dni", dni);
        TypedQuery<Usuario> tq = em.createQuery("SELECT u FROM Usuario u", Usuario.class);

        for (Usuario u : tq.getResultList()) {
            if (nombre.equals(u.getNombre())) {
                return u;
            }
        }
        return null;
    }
    
    @Override
    public List<Aviso> getNAvisos(){
        Query query = em.createQuery("SELECT av FROM Avisos AS av WHERE av.Estado=enCola");
        return (List<Aviso>) query.getResultList();
    }

    @Override
    public List<Aviso> getLAvisos(){
        Query query = em.createQuery("SELECT av FROM Avisos AS av WHERE && av.Estado=abierto || av.Estado=despachadoMovilidad || av.Estado=generadaOT");
        return (List<Aviso>) query.getResultList();
    }

    @Override
    public List<Aviso> getCAvisos(){
       Query query = em.createQuery("SELECT av FROM Avisos AS av WHERE av.Estado=cerrado");
        return (List<Aviso>) query.getResultList(); 
    }
    
    @Override
    public List<Aviso> getAllAvisos(){
       Query query = em.createQuery("SELECT av FROM Avisos");
        return (List<Aviso>) query.getResultList(); 
    }
    
    @Override
    public Afectado getAfectados(Afectado a){
        Afectado af= em.find(Afectado.class, a.getId_afectado());
        return af; 
    }
    
    @Override
    public Aviso buscaAviso(Aviso a){
        Aviso av= em.find(Aviso.class, a.getId_Aviso());
        return av;
    }
}