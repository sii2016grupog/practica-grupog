/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author alumno
 */
@Entity
public class Orden implements Serializable {
    public enum TiposEstado{abierto,despachadoMovilidad,cerrado};
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_Orden;
    @Column (nullable = false)
    private String Descripcion;
    @Column (nullable = false)
    private TiposEstado Estado;
    @Column (nullable = false)
    private int Fecha_inicio;
    private int Fecha_fin;
    
    @ManyToMany (mappedBy="trabOnOrden")
    @JoinTable(name = "jnd_trab_brig", joinColumns = @JoinColumn(name = "trab_fk"), inverseJoinColumns = @JoinColumn(name = "brig_fk"))
    private List<Trabajador> trabajadores;
    
    @ManyToOne
    @JoinColumn (nullable = false)
    private Aviso aviso_O;
    
    @ManyToOne
    @JoinColumn (nullable = false)
    private Supervisor supervisor_O;
    
    
    
    public Long getId_Orden() {
        return id_Orden;
    }
    
    public String getDescripcion() {
        return Descripcion;
    }
    
    public TiposEstado getEstado(){
        return Estado;
    }

    public int getFecha_inicio() {
        return Fecha_inicio;
    }

    public int getFecha_fin() {
        return Fecha_fin;
    }

    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public Aviso getAviso_O() {
        return aviso_O;
    }

    public Supervisor getSupervisor_O() {
        return supervisor_O;
    }

    public void setId_Orden(Long id_Orden) {
        this.id_Orden = id_Orden;
    }
    
    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
    public void setEstado(TiposEstado Estado) {
        this.Estado = Estado;
    }
    
    public void setFecha_inicio(int Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }
    
    public void setFecha_fin(int Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    public void setAviso_O(Aviso avisos) {
        this.aviso_O = avisos;
    }

    public void setSupervisor_O(Supervisor supervisor_O) {
        this.supervisor_O = supervisor_O;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Orden != null ? id_Orden.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orden)) {
            return false;
        }
        Orden other = (Orden) object;
        if ((this.id_Orden == null && other.id_Orden != null) || (this.id_Orden != null && !this.id_Orden.equals(other.id_Orden))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Orden[ id=" + id_Orden + " ]";
    }
    
}
