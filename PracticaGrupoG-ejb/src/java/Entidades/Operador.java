/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Trethtzer
 */
@Entity
public class Operador extends Usuario {
    private static final long serialVersionUID = 1L;
    @OneToMany(mappedBy="operador_N")
    private List<Notificacion> notificaciones;
    @OneToMany(mappedBy="operador_A")
    private List<Aviso> avisos;

    public Operador(int i, String n, String a, String c, String calle, String dni, String tlf) {
        super(i, n, a, c, calle, dni, tlf);
        super.setRol(Rol.OPERADOR); 
    }

    public List<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public List<Aviso> getAvisos() {
        return avisos;
    }

    public void setNotificaciones(List<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public void setAvisos(List<Aviso> avisos) {
        this.avisos = avisos;
    }
    
}
