/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Trethtzer
 */
@Entity
public class Trabajador extends Usuario {
    private static final long serialVersionUID = 1L;

    private Integer brigada;
    @ManyToMany
    @JoinTable(name = "jnd_trab_brig", joinColumns = @JoinColumn(name = "trab_fk"), inverseJoinColumns = @JoinColumn(name = "brig_fk"))
    private List<Orden> trabOnOrden;

     public Trabajador(int i, String n, String a, String c, String calle, String dni, String tlf) {
        super(i, n, a, c, calle, dni, tlf);
        super.setRol(Rol.TRABAJADOR); 
    }

    public Integer getBrigada() {
        return brigada;
    }

    public List<Orden> getTrabOnOrden() {
        return trabOnOrden;
    }

    public void setBrigada(Integer brigada) {
        this.brigada = brigada;
    }

    public void setTrabOnOrden(List<Orden> trabOnOrden) {
        this.trabOnOrden = trabOnOrden;
    }
    
    
}
